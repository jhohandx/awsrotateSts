#%%
import boto3
import json, os
from botocore.retries import bucket

cnf_client = boto3.client('cloudformation', region_name='us-east-1')
sts_client = boto3.client('sts', region_name='us-east-1')
s3_client = boto3.client('s3', region_name='us-east-1')

#%%
account_id = sts_client.get_caller_identity()["Account"]

stacks=["rotatests"]
def checkstack():
    rotatestsexist=["rotatests"]
    stackfound=""
    for row in stacks:
        try:
            response = cnf_client.describe_stacks(
                StackName=row
            )
            stackfound=response['Stacks'][0]['StackName']
        except Exception as e:
            stackfound="false"

        if stackfound == "rotatests":
            stackfound="true"
        else:
            stackfound="false"
    return stackfound
rotatestsExist=checkstack()

if rotatestsExist == "true":
    print("true")
else:
    print("false")
