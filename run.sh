  if [ false = false ];
  then
  id=$(aws sts get-caller-identity --query Account --output text)
  echo "start rotatests deploy on account $id"
  aws s3 ls | grep  rotatests-$id || aws s3api create-bucket --bucket rotatests-$id --region us-east-1
  S3_BUCKET=rotatests-$id
  REGION=us-east-1
  ls
  aws s3 cp /home/runner/work/awsrotatests/awsrotatests/rotatests.zip s3://$S3_BUCKET/rotatests.zip
  aws s3 cp /home/runner/work/awsrotatests/awsrotatests/infra-template.yaml s3://$S3_BUCKET/infra-template.yaml
  aws cloudformation create-stack \
    --stack-name rotatests \
    --template-url https://$S3_BUCKET.s3.amazonaws.com/infra-template.yaml \
    --parameters \
        ParameterKey=CodeS3Bucket,ParameterValue=$S3_BUCKET \
        ParameterKey=ProjectId,ParameterValue=rotatests \
        ParameterKey=RepositoryName,ParameterValue=rotatests \
        ParameterKey=CodeS3Key,ParameterValue=rotatests.zip \
    --capabilities CAPABILITY_NAMED_IAM  
  else
  echo 'rotatests already exist'
  exit
  fi 