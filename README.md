How deploy rotatests
==============================================

Enable trusted access with AWS Organizations
==============================================
https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/stacksets-orgs-enable-trusted-access.html


retrive credentials on management account and paste on terminal
--------------------------------------------------------------

```
export AWS_ACCESS_KEY_ID="ASIAYZMZ4Q777LWHPW5L"
export AWS_SECRET_ACCESS_KEY="twScxVANL/NMRfdE1np9r+82jLQEa1w3oYjZVkyD"
export AWS_SESSION_TOKEN="IQoJb3JpZ2luX2VjEJT//////////wEaCXVzLWVhc3QtMSJHMEUCIATgNHZSQmuqjpxY51kkHFr8YCVMjyQXnCMqYcqarVYFAiEA5Hhqi+wSLqPql8SfY/9cDF/KI7AjpjINZhyvoDpmOfwqqAMIrf//////////ARAAGgw2MDQzMDI1MTAwNzkiDNT6CwsvCeN8fohbCCr8AhozH1gJiVa2RmvABMTfeCUYmYqQ3RLBBPeCQqeOqyRur5cEqgs3xObCYVOCbYUWAD6GfBZgzD5w+fexBvj9NJbExC7tPg8Mqe30dzdbjyJl+8CWSLIb27/ZDyUBosIJVj2Ez8QwhyM/wNmxNp8XEuI3jZEMs59jI5hc/CRnbkPLn8UIwHeLjT0/PcJzTwpPWh3t7UytrSs1n4hVKWEYtP660uKvCFRngeiQYQ+P3rUJYvg3IubaOF+1PtpjnnoZs/OPvQ1JPLQEkydhU2Tfoowdp4nCUuSOZjnxBErWwFzmEla48DnyaDVaajfXiwIzPVjvcUgGqcHWkFYez2ey3TxETUsbLnMtBm+GqumDOKMnObXXYNIjHqP1Q6yQ/8oevvueKCvDAwWel7P4dcSR+c9LWzmL8Bcfm0fCVKVwRwyhOyzGvJp5rLII95OUvXucZEEBvf37JrRCESeGe+Jt0isJC2eWUXepphRWeCJqeRsA4RJhDr0vQXII/XT1MPWBxogGOqYBpcy0ELq83uwVvVNdD11WafhrE4KVUI9O1FNIShmFqd4loiVp6Z1hheUN3uL82F+Mt+W9mAOho0E7q/4JHcoedI0CfBMnMR7X8pr2Ic+lC3PpP4j/zBO/XGpb0q7p97OwmP1vNq1csEhiy/O8UKJ6orlgYtT3z0AuqHY9heS8ipMMHtW5foll4j1Jgd0XgI6vV61kkyCoF+AX5mGiVohmzaBHZX9rjQ=="

```
on the rotatests path using this command
--------------------------------------------------------------
```
    id=$(aws sts get-caller-identity --query Account --output text)
    echo "start rotatests deploy on account $id"
    aws s3 ls |grep  rotatests-$id || aws s3api create-bucket --bucket rotatests-$id --region us-east-1
    S3_BUCKET=rotatests-$id
    REGION=us-east-1
    aws s3 cp ./rotatests.zip s3://$S3_BUCKET/rotatests.zip
    aws s3 cp ./infra-template.yaml s3://$S3_BUCKET/infra-template.yaml
    aws cloudformation create-stack \
    --stack-name rotatests \
    --template-url https://$S3_BUCKET.s3.amazonaws.com/infra-template.yaml \
    --parameters \
        ParameterKey=CodeS3Bucket,ParameterValue=$S3_BUCKET \
        ParameterKey=ProjectId,ParameterValue=rotatests \
        ParameterKey=RepositoryName,ParameterValue=rotatests \
        ParameterKey=CodeS3Key,ParameterValue=rotatests.zip \
    --capabilities CAPABILITY_NAMED_IAM 
```

Lambdas Deploy
----------------------------
```
#rotatests/lambdas
sam deploy --template-file lambdas-template.yaml --capabilities CAPABILITY_NAMED_IAM
```

FirstLevel Deploy
----------------------------

edit organization root id, and tags
stack-sets\FirstLevel\governance\settings.json
stack-sets\FirstLevel\ssm-parameters\settings.json

```
    {
    "DeploymentTargets": {
        "OrganizationalUnitIds": ["edit-here"]
    },
    "Regions": ["us-east-1"],
    "OperationPreferences": {
        "FailureTolerancePercentage": 100,
        "MaxConcurrentCount": 10
    },
    "Tags": [ 
        {
            "Key": "bancolombia:owner",
            "Value": "cloud"
        },
        {
            "Key": "bancolombia:cost-center",
            "Value": "c103500003"
        },
        {
            "Key": "bancolombia:pmo",
            "Value": "c103500003"
        },
        {
            "Key": "bancolombia:business-service",
            "Value": "cloud"
        },
        {
            "Key": "bancolombia:application-code",
            "Value": "aw0000"
        },
        {
            "Key": "bancolombia:environment",
            "Value": "pdn"
        },
        {
            "Key": "bancolombia:project-name",
            "Value": "cloud"
        }
    ]
    }
```
save changes and update the repository

rotatests delete
----------------------------
```
aws cloudformation delete-stack --stack-name deploy-on-root
aws cloudformation delete-stack --stack-name stack-sets-lambdas
aws cloudformation delete-stack --stack-name rotatests
```

SCP DenyCloudformationrotatestss
----------------------------
Politica para evitar que se despliegue inicialmente en las cuentas de pragma los stack de rotatestss

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "DenyCloudformationrotatestss",
            "Effect": "Deny",
            "Action": [
                "cloudformation:CreateStack"
            ],
            "Resource": [
                "arn:aws:cloudformation:*:*:stack/*/*"
            ],
            "Condition": {
                "StringLike": {
                    "aws:PrincipalARN": [
                        "arn:aws:iam::*:role/AWSCloudFormationStackSetExecutionRole",
                        "arn:aws:iam::*:role/stacksets-exec-*"
                    ]
                }
            }
        }
    ]
}

```
